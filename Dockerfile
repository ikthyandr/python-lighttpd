FROM debian:buster
MAINTAINER Arnab Somadder <arnab.s@lowerentropy.com>

RUN apt-get update\
	&& apt-get -y upgrade\
	&& apt-get -y install python lighttpd nano python-pip python-memcache\
	&& pip install pymongo==3.11\
	&& pip install requests\
	&& pip install mailjet_rest\
	&& lighty-enable-mod cgi

COPY lighttpd.conf /etc/lighttpd/lighttpd.conf
COPY 10-cgi.conf /etc/lighttpd/conf-available/10-cgi.conf
COPY html/html /var/www/html/

EXPOSE 80
EXPOSE 443
ENTRYPOINT ["/usr/sbin/lighttpd"]
CMD ["-f", "/etc/lighttpd/lighttpd.conf", "-D", "start"]